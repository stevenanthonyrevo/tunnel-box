## Tunnel Box 

An operation command line tool for docker built to host ports, ips, containers and images within one centralized location on a single production machine. 

| CMD    | FUNCTION                                                                              |
|--------|---------------------------------------------------------------------------------------|  
| START  | will trigger a function to start a container from a new built image.                  | 
| SHOW   | will trigger a function to show all containers currently running on operating system. |
| DOWN   | will trigger a function to prune and stop all containers from a built image.          |
| REBOOT | will trigger a function to reload the containers with fresh built images.             |